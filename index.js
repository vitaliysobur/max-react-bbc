'use strict';

var ReactDOM = require('react-dom');
var MaxReactDOM = ReactDOM;

MaxReactDOM.render = (function (fn) {
  return function () {
    var args = arguments;
    console.log('MaxReactDOM.render', args);
    return fn.apply(this, args);
  }
})(MaxReactDOM.render);

module.exports = MaxReactDOM;